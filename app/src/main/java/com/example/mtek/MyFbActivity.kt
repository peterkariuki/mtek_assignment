package com.example.mtek

import androidx.appcompat.app.AppCompatActivity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button

class MyFbActivity : AppCompatActivity() {
    private var FbOkbtn: Button? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_fb)

        FbOkbtn = findViewById<View>(R.id.FbOkbtn) as Button
        FbOkbtn!!.setOnClickListener { openFb() }
    }

    fun openFb() {
        val i = Intent(this, Main13Activity::class.java)
        startActivity(i)
    }


}

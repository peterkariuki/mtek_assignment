package com.example.mtek

import android.util.Log

import java.util.zip.GZIPInputStream

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class PingRepository {
    internal var repo = Retrofit.Builder().baseUrl("").addConverterFactory(GsonConverterFactory.create()).build()
    private val pingWebSerice: iPingWebService

    init {
        val repo = Retrofit.Builder().baseUrl(":https://hillcroftinsurance.com:8445/").addConverterFactory(GsonConverterFactory.create()).build()
        this.pingWebSerice = repo.create(iPingWebService::class.java!!)
    }

    fun getStatus() {
        this.pingWebSerice.status.enqueue(object : Callback<ServerStatus> {
            override fun onResponse(call: Call<ServerStatus>, response: Response<ServerStatus>) {
                val r = response.body()
                //to string - getGroup
                Log.e("iPING", r!!.group!!)
            }

            override fun onFailure(call: Call<ServerStatus>, t: Throwable) {

            }
        })

    }
}

package com.example.mtek

import androidx.appcompat.app.AppCompatActivity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button

class Login : AppCompatActivity() {
    private var fb1: Button? = null
    private var twita1: Button? = null
    private var email1: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        fb1 = findViewById<View>(R.id.fb1) as Button
        fb1!!.setOnClickListener { openFb() }

        twita1 = findViewById<View>(R.id.twita1) as Button
        twita1!!.setOnClickListener { openTwita() }

        email1 = findViewById<View>(R.id.email1) as Button
        email1!!.setOnClickListener { openEmail() }


    }

    fun openFb() {
        val lgn = Intent(this, Facebk::class.java)
        startActivity(lgn)
    }

    fun openTwita() {
        val intent = Intent(this, twitter1::class.java)
        startActivity(intent)
    }

    fun openEmail() {
        val intent = Intent(this, Eml::class.java)
        startActivity(intent)
    }

}

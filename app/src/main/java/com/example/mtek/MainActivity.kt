package com.example.mtek

import androidx.appcompat.app.AppCompatActivity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button

class MainActivity : AppCompatActivity() {
    private var btnLogin: Button? = null

    private var btnTwitter: Button? = null

    private var Fb: Button? = null
    private var Eml: Button? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Eml = findViewById<View>(R.id.Eml) as Button


        Fb = findViewById<View>(R.id.Fb) as Button
        Fb!!.setOnClickListener { openFb() }




        btnTwitter = findViewById<View>(R.id.btnTwitter) as Button
        btnTwitter!!.setOnClickListener { openTwitter() }



        btnLogin = findViewById<View>(R.id.btnLogin) as Button
        btnLogin!!.setOnClickListener { openLogin() }


    }


    fun openLogin() {
        val log = Intent(this, Login::class.java)
        startActivity(log)
    }

    fun openTwitter() {
        val twitter = Intent(this, twitter1::class.java)
        startActivity(twitter)

    }


    fun openFb() {
        val intent = Intent(this, Facebk::class.java)
        startActivity(intent)
    }

    }




package com.example.mtek

import androidx.appcompat.app.AppCompatActivity

import android.os.Bundle
import android.widget.Toast

import com.luseen.spacenavigation.SpaceItem
import com.luseen.spacenavigation.SpaceNavigationView
import com.luseen.spacenavigation.SpaceOnClickListener

class Navigation : AppCompatActivity() {
    internal var navigationView: SpaceNavigationView
        get() {
            TODO()
        }
        set(value) {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)

        navigationView = findViewById(R.id.space)
        navigationView.initWithSaveInstanceState(savedInstanceState)
        navigationView.addSpaceItem(SpaceItem("", R.drawable.ic_home_black_24dp))
        navigationView.addSpaceItem(SpaceItem(" ", R.drawable.ic_add_alert_black_24dp))
        navigationView.addSpaceItem(SpaceItem("", R.drawable.ic_settings_black_24dp))
        navigationView.addSpaceItem(SpaceItem(" ", R.drawable.ic_error_outline_black_24dp))

        navigationView.setSpaceOnClickListener(object : SpaceOnClickListener {
            override fun onCentreButtonClick() {
                Toast.makeText(this@Navigation, "onCentreButtonClick", Toast.LENGTH_SHORT).show()
                navigationView.setCentreButtonSelectable(true)
            }

            override fun onItemClick(itemIndex: Int, itemName: String) {
                Toast.makeText(this@Navigation, "$itemIndex $itemName", Toast.LENGTH_SHORT).show()
            }

            override fun onItemReselected(itemIndex: Int, itemName: String) {
                Toast.makeText(this@Navigation, "$itemIndex $itemName", Toast.LENGTH_SHORT).show()
            }
        })


    }
}

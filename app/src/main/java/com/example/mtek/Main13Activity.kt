package com.example.mtek

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

import android.app.Notification
import android.os.Bundle
import android.view.MenuItem

import com.google.android.material.bottomnavigation.BottomNavigationView

class Main13Activity : AppCompatActivity() {
    private val navListener = BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
        var selectedFragment: Fragment? = null

        when (menuItem.itemId) {
            R.id.nav_settings -> selectedFragment = SettingsFragment()

            R.id.nav_notification -> selectedFragment = NotificationsFragment()
            R.id.nav_home -> selectedFragment = HomeFragment()
        }
        supportFragmentManager.beginTransaction().replace(R.id.frag_container,
                selectedFragment!!).commit()
        true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main13)

        val bottom_nav = findViewById<BottomNavigationView>(R.id.bottom_navigation)
        bottom_nav.setOnNavigationItemSelectedListener(navListener)

        supportFragmentManager.beginTransaction().replace(R.id.frag_container,
                HomeFragment()).commit()

    }
}

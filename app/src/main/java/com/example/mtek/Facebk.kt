package com.example.mtek

import androidx.appcompat.app.AppCompatActivity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button

class Facebk : AppCompatActivity() {
    private var Ctnfb: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_facebk)

        Ctnfb = findViewById<View>(R.id.Ctnfb) as Button
        Ctnfb!!.setOnClickListener { openLogin2() }
    }

    fun openLogin2() {
        val intent = Intent(this, MyFbActivity::class.java)
        startActivity(intent)
    }
}

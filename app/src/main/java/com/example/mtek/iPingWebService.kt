package com.example.mtek

import retrofit2.Call
import retrofit2.http.GET

interface iPingWebService {
    @get:GET
    val status: Call<ServerStatus>
}
